# wavingPaw
-----------
This is a not-so-intended screensaver. Use at your own risk!

## Dependencies
---------------
- gloss 12.0

## Compile and Launch
------
An X11 screensaver will soon be added. For now please use stack or cabal to compile ``app/Main.hs``