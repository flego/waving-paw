
module Main where

import Graphics.Gloss

main :: IO ()
main    = animate FullScreen black picture

picture time = translate 0 (-300) $ cat time

cat time = pictures $ [catbody time, cathead time, cattail time, catleftfoot time, catrightfoot time, catlefteye time, catrighteye time, catnose time, catleftpaw time, catrightarm time, catrightsubarm time, catrightpaw time, catn1 time, catn2 time, catn3 time, catn4 time, catn5 time, catn6 time]

catbody time = color creme $ Polygon [(60,0), (90,10), (120,60), (70,230), (30,250), (-30,250), (-70,230), (-120,60), (-90,10), (-60,0)]
cathead time = translate 0 300 $ color creme $ circleSolid 70
cattail time = translate (-50) 0 $ color creme $ Polygon [(-50,10) ,(-70,0), (-80,0), (-90,10), (-100,30), (-90,50), (-70,60), (-90,40), (-90,30), (-80,20), (-50,30)]
catleftfoot time = translate (-60) 40 $ color black $ circleSolid 20
catrightfoot time = translate   60  40 $ color black $ circleSolid 20
catlefteye time  = translate ((-20) + 5 * (swing time)) 320 $ color black $ circleSolid 8
catrighteye time = translate   (20  + 5 * (swing time)) 320 $ color black $ circleSolid 8
catnose time = translate 0 280 $ color black $ circleSolid 5
catleftpaw time = translate (-40) 180 $ color black $ circleSolid 15
catrightarm time = color creme $ Polygon [(0,190), (180,190), (180,170), (0,170)]
catrightsubarm time = color creme $ Polygon [(160,180), (160,(200 - 60 * (swing time))), (180,(200 - 60 * (swing time))), (180, 180)]
catrightpaw time = translate 170 (200 - 60 * (swing time)) $ color creme $ circleSolid 15
catn1 time = translate 10 270    $ rotate   30  $ color black $ Polygon [(0,0), (20,0), (20,2), (0,2)]
catn2 time = translate 12 280                   $ color black $ Polygon [(0,0), (20,0), (20,2), (0,2)]
catn3 time = translate 10 290    $ rotate (-30) $ color black $ Polygon [(0,0), (20,0), (20,2), (0,2)]
catn4 time = translate (-28) 300 $ rotate   30  $ color black $ Polygon [(0,0), (20,0), (20,2), (0,2)]
catn5 time = translate (-30) 280                $ color black $ Polygon [(0,0), (20,0), (20,2), (0,2)]
catn6 time = translate (-28) 260 $ rotate (-30) $ color black $ Polygon [(0,0), (20,0), (20,2), (0,2)]

-- skin color
creme :: Color
creme = makeColorI 250 255 196 255 

-- paw and eye movement
swing :: Float -> Float
swing time = sin $ 5 * time
